<?php

namespace Framework\FileSystem;

use DirectoryIterator;
use Framework\String\Str;

class FileManager
{

    public function __construct()
    {
        
    }

    /**
     * We will Grab All files Given from Directory
     * 
     * @return Arrays of DirectoryIterator
     */
    public static function ls($directory)
    {
        $dirIterator = new DirectoryIterator($directory);
        $files = [];

        foreach ($dirIterator as $key => $file) {
            if ($file->isDot())
                continue;

            $files[] = new File($file->getRealPath());
        }

        return $files;
    }

    /**
     * We will Grab All files from Disc
     * 
     * @return Arrays of \stdClass
     */
    public static function files($directory)
    {
        $dirIterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new \RecursiveIteratorIterator($dirIterator, \RecursiveIteratorIterator::SELF_FIRST);
        $files = [];
        $sl = 0;

        foreach ($iterator as $file) {
            $files[$sl] = $file->getPathname();
            $sl++;
        }

        return $files;
    }

    public function singleFile($directory)
    {
        $dirIterator = new \DirectoryIterator($directory);

        foreach ($dirIterator as $file) {
            if ($file->isDot() === false && $file->isFile() === true) {
                return $file->getFilename();
            }
        }
    }

    public static function returnIterator($directory)
    {
        return new \DirectoryIterator($directory);
    }

    public static function recursiveIterator($directory)
    {
        $dirIterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new \RecursiveIteratorIterator($dirIterator, \RecursiveIteratorIterator::SELF_FIRST);

        return $iterator;
    }

    public static function returnUniquePath($filePath)
    {
        $pathInfo = pathinfo($filePath);
        $dirname = isset($pathInfo['dirname']) ? trim($pathInfo['dirname'], '.') : '';
        $fileName = isset($pathInfo['filename']) ? $pathInfo['filename'] : '';
        $extension = isset($pathInfo['extension']) ? $pathInfo['extension'] : '';

        $fileName = Str::limit(Str::alphaNum($fileName), 64);

        $able = false;
        while ($able === false) {
            if (file_exists($dirname . DS . $fileName . '.' . $extension)) {
                $fileName .= '-1';
            } else {
                $able = true;
            }
        }

        return $dirname . DS . $fileName . '.' . $extension;
    }

    public static function randomFile($directory)
    {

        $iterator = self::returnIterator($directory);
        $files = [];

        if (!empty($iterator)) {
            foreach ($iterator as $file) {
                if ($file->isDot() === false && $file->isFile()) {
                    $filename = $file->getBasename();

                    $files[] = $directory . DS . $filename;
                }
            }
        }
        if (empty($files)) {
            return false;
        }
        $randomKey = random_int(0, count($files) - 1);

        return $files[$randomKey];
    }

    public static function createDIR($directory, $recursive = true)
    {
        if (is_dir($directory) === false) {
            mkdir($directory, 0755, $recursive);
        }

        return is_dir($directory);
    }

    public static function checkDIR($directory)
    {
        if (self::createDIR($directory) === false) {
            throw new \Exception('Failed to create new Directory. Check your directory permission.');
        }
        return $directory;
    }

    public static function appendToFile($data, $file)
    {
        return file_put_contents($file, $data . PHP_EOL, FILE_APPEND | LOCK_EX);
    }

    public static function unlink($file)
    {
        if (file_exists($file)) {
            return unlink($file);
        }

        return false;
    }

    public static function moveToTrash($file)
    {
        $trashDIR = ROOT_DIR . DS . 'logs' . DS . 'trash';
        self::checkDIR($trashDIR);
        $targetPath = self::returnUniquePath($trashDIR . DS . pathinfo($file, PATHINFO_BASENAME));

        if (file_exists($file)) {
            rename($file, $targetPath);
        }

        return file_exists($targetPath);
    }

    public static function totalLine($file)
    {
        if (file_exists($file) === false) {
            return false;
        }
        $fileObj = new \SplFileObject($file);
        $fileObj->seek(PHP_INT_MAX);
        return $fileObj->key();
    }

    public static function readLastLine($file, $targetLine = 1)
    {
        if (file_exists($file) === false) {
            return false;
        }
        $fileObj = new \SplFileObject($file);
        $fileObj->seek(PHP_INT_MAX);
        $totalLines = $fileObj->key();
        $targetOffset = ($totalLines > $targetLine) ? ($totalLines - $targetLine) : 0;

        $fileReader = new \LimitIterator($fileObj, $targetOffset);
        $lines = [];
        foreach ($fileReader as $line) {
            $lines[] = $line;
        }

        return $lines;
    }

    public static function clearDirectory($directory, $self = false)
    {
        $iterator = self::recursiveIterator($directory);
        $isCleaned = false;
        $directories = [];

        foreach ($iterator as $file) {
            if ($file->isFile()) {
                unlink($file->getPathname());
                $isCleaned = true;
            } elseif ($file->isDir()) {
                $directories[] = $file->getPathname();
            }
        }

        if (!empty($directories)) {
            foreach ($directories as $dir) {
                if (is_dir($dir)) {
                    rmdir($dir);
                }
            }
        }

        if ($self === true) {
            rmdir($directory);

            return is_dir($directory);
        }

        return $isCleaned;
    }

    public function createTime($file)
    {
        if (file_exists($file)) {
            return filectime($file);
        }

        return 0;
    }

    public function modifyTime($file)
    {
        if (file_exists($file)) {
            return filemtime($file);
        }

        return 0;
    }

    public function mimeType($filePath)
    {
        if (!is_file($filePath)) {
            return false;
        }

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $filePath);
        finfo_close($finfo);

        return $mime;
    }

}
